import React, { Fragment } from "react";
import DashboardMessage from "../displays/DashboardMessage";

class Dashboard extends React.Component {
  state = {
    display: null,
  };

  componentDidMount() {}

  render() {
    return (
      <Fragment>
        <DashboardMessage />
      </Fragment>
    );
  }
}

export default Dashboard;

// const Dashboard = () => (
//     <React.Fragment>
//       <DashboardMessage />
//     </React.Fragment>
//   );
