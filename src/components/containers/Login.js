import React from "react";
import LoginForm from "../forms/LoginForm";

class Login extends React.Component {
  //Init state
  state = {
    loggedIn: null,
  };

  componentDidMount() {
    //Rendered (OnInit)
  }
  render() {
    return (
      <React.Fragment>
        <h1> Login to survey puppy</h1>

        <LoginForm />
      </React.Fragment>
    );
  }
}

export default Login;

// const Login = () => {
//   return (
//     <div>
//       <h1> Login to survey puppy</h1>

//       <LoginForm/>
//     </div>
//   );
// };
