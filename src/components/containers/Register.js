import React, { Fragment } from "react";
import RegisterForm from "../forms/RegisterForm";

class Register extends React.Component {
  state = {
    isRegistered: null,
  };

  componentDidMount() {}
  render() {
    return (
      <Fragment>
        <h1>Register to Survey Puppy</h1>
        <RegisterForm />
      </Fragment>
    );
  }
}

export default Register;

// const Register = () => (
//   <div>
//     <h1>Register to Survey Puppy</h1>

//     <RegisterForm/>
//   </div>
// );
