import React from "react";

const RegisterForm = () => {
  return (
    <form>
      <div>
        <label>username</label>
        <input type="text" placeholder="Enter a username" />
      </div>

      <div>
        <label>password</label>
        <input type="password" placeholder="Enter a password" />
      </div>

      <div>
        <button type="button" onClick={ () => console.log("I am clicked") }>Register</button>
      </div>
    </form>
  );
};

export default RegisterForm;
